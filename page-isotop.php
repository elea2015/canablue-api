<?php /* Template Name: isotop*/ get_header(); ?>

<?php
	//get page slug to define loop post type and content to show
    global $post;
    $post_slug=$post->post_name;

    if ($post_slug == 'new-construction' ):

	   $the_post_type = 'newdev';

	elseif($post_slug == 'economy'):

	   $the_post_type = 'first-home';

	elseif($post_slug == 'resale'):

	   $the_post_type = 'buy';

	elseif($post_slug == 'lots'):

	   $the_post_type = 'lot';

	endif;
?>
	<section class="buySection">
		<div class="container text-center">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col">
					<h2><?php the_field('heading') ?></h2>
					<div class="ownersSericeList"><?php the_field('pitch') ?></div>
					<div class="container searchForm wow fadeInUp"><?php echo do_shortcode( '[searchandfilter add_search_param="1" fields="location,type"  post_types="'.$the_post_type.'" submit_label="Search" all_items_labels="All Locations, All Types"]' ); ?></div>
					<?php if(!is_page(array(349,230,685,10))) :?>
					<br><br>
					<div class="bd-example">
					<!-- /btn-group -->	
					  <div id="priceGrid" class="btn-group button-group">
					    <div class="dropdown">
						  <button class="btn btn-light dropdown-toggle filtersGrid" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Price Range
						  </button>
						  <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
						  	<button class="dropdown-item button is-checked" data-price="*">show all range</button>
						    <button class="dropdown-item" data-price="numberGreaterThan50000" type="button"> > 50k</button>
						    <button class="dropdown-item" data-price="numberGreaterThan100000" type="button"> > 100k</button>
						    <button class="dropdown-item" data-price="numberGreaterThan150000" type="button"> > 150k</button>
						    <button class="dropdown-item" data-price="numberGreaterThan200000" type="button"> > 200k</button>
						  </div>
						</div>
					  </div>
					  <!-- /btn-group -->
					  <!-- /btn-group -->	
					  <div id="locationGrid" class="btn-group button-group">
					    <div class="dropdown">
						  <button class="btn btn-light dropdown-toggle filtersGrid" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Location
						  </button>
						  <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
						  	<button class="dropdown-item button is-checked" data-location="*">Show All Locations</button>
						    <button class="dropdown-item" data-location=".cortecito" type="button">Cortecito</button>
						    <button class="dropdown-item" data-location=".cocotal" type="button">Cocotal</button>
						    <button class="dropdown-item" data-location=".costa-bavaro" type="button">Costa Bavaro</button>
						    <button class="dropdown-item" data-location=".los-corales" type="button">Los Corales</button>
						  </div>
						</div>
					  </div>
					  <!-- /btn-group -->
					</div>
				<?php endif;?>
					<!-- filter -->
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</section>
	<?php the_field('pricing');?>
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	<section class="buySection buyListing">
		<div class="container">
		<?php
			$orig_query = $wp_query;

			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			//Query Arguments
			$args = array(
				'post_type' => array($the_post_type),
				//'posts_per_page' => 18,
        		//'paged' => $paged,
        		//'orderby' => 'rand',
        		//'order' => 'ASC'
			);
			$wp_query = new WP_Query($args); 

		?>

		<?php if($wp_query->have_posts()) ?>
		   <div class="property-cards-container">
		     <?php  while ( $wp_query->have_posts() ): $wp_query->the_post(); $a++;?>
		        	<?php 
		        		$propertyLink = get_post_permalink();
		        		$propertyId =	get_the_ID();

		        		$time = get_field('time');

		        		//Get Firs tag
		        		$allposttags = get_the_tags();
		                $i=0;
		                if ($allposttags) {
		                    foreach($allposttags as $tags) {
		                        $i++;
		                        if (1 == $i) {
		                            $firsttag = $tags->name;
		                        }
		                    }
		                }
		                //Get Location Slug
		                $terms = get_the_terms( $propertyId, 'location');
						 $count = count($terms);
						 if ( $count > 0 ){
						     foreach ( $terms as $term ) {
						       $locationGrid = $term->slug;

						     }
						 }
		        	 ?>
		          	<!-- Price Card -->
					<div class="card grid-item grid-item--width2 transition <?php echo $locationGrid; ?>">
						<a href="<?php echo $propertyLink; ?>">
							<div class="card-img-top-container">
								<img class="card-img-top" src="<?php the_post_thumbnail_url('feature'); ?>" alt="Card image cap">
							</div>
							<h5><span class="badge badge-primary"><?php echo $firsttag; ?></span></h5>
							<div class="card-body">
								<h4 class="card-title"><?php the_title(); ?></h4>
								<h6 class="card-subtitle mb-2 text-muted location"><?php echo get_the_term_list( $propertyId, 'location'); ?></h6>
								<p class="card-text"><?php if($the_post_type == 'newdev'):?>Prices starting at<?php endif;?> $<?php echo number_format(get_field('price'));?>
									<?php if ( $time == 'Month' ): echo " /Month"; elseif ( $time == 'Night' ): echo " /Night"; elseif ( $time == 'Week' ): echo " /Week"; endif; ?>
									<?php
									 $postObj = get_post_type_object( $the_post_type ); 
									if ( $postObj->labels->singular_name == 'Lot' ): echo " /per square meter "; endif; ?>
								</p>
								<?php if($the_post_type !== "lot"): ?>
								<span class="card-link disabled"><?php echo get_field('bedrooms')?> <i class="fa fa-bed" aria-hidden="true"></i></span>
								<span class="card-link disabled"><?php echo get_field('bathrooms')?> <i class="fa fa-bath" aria-hidden="true"></i></span>
								<span style="display:none;" class="number"><?php the_field('price'); ?></span>
								<?php endif; ?>
								<?php if($the_post_type == "newdev"): ?>
									<?php if(get_field('sale_status') == 'Only several units left'): ?><span class="badge badge-warning pull-right">Only several units left</span>
									<?php elseif(get_field('sale_status') == 'Sold out'): ?><span class="badge badge-success pull-right">Sold out</span>
									<?php endif; ?>
								<?php endif?>
							</div>
						</a>
					</div>
					<!-- end price Card -->
		     <?php  //if($a % 3 === 0) :  echo '</div> <div class="row priceRow">'; endif; ?>
			<?php endwhile; ?>
			</div>

			<div class="nav-previous alignleft"><?php next_posts_link( 'Older posts' ); ?></div>
			<div class="nav-next alignright"><?php previous_posts_link( 'Newer posts' ); ?></div>
			<?php wp_reset_query(); ?> 
			</div>
		</div>
	</section>

	<?php endwhile; endif;  $wp_query = $orig_query; ?>

	<?php 
	get_template_part('include/optin');
	?>

	<?php get_template_part('include/zonas')?>

<?php get_footer(); ?>