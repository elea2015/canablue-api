<?php 
get_header();

//fields
$tag_line       =   get_field('tag_line');
$video          =   get_field('video');
$gallery        =   get_field('gallery_images');
$overview       =   get_field('overview');
$overview_img   =   get_field('overview_images');
$location_title =   get_field('location_title');
$location_ben   =   get_field('location_benefits');
$investment     =   get_field('investment');
$keyFeatures    =   get_field('key_features');
$terms          =   get_field('location');
//include map
get_template_part('include/map');
?>


    <section class="community-hero my-5">
        <div class="container">
            <div class="row">
                <div class="col-md-5 community-intro">
                    <small class="blue">Community</small>
                    <h1 class="blue">Welcome to <?php the_title();?></h1>
                </div>
                <div class="col-md-7">
                   <div class="community-video-container"> 
                        <?php the_post_thumbnail('property-screenshot'); // Fullsize image for the single post ?>
                        <?php if(get_field('video')):?>
                        <a class="unitCommunityVideo" href="#" data-toggle="modal" data-target="#videoModal">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/icon/play-button.svg" alt="">
                        </a>
                        <!-- modal -->
                        <div class="modal fade" id="videoModal" tabindex="-1" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title blue"><?php the_title(); ?></h5>
                                        <button onclick="stop()" type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body embed-responsive embed-responsive-16by9">
                                        <?php the_field('video'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal -->
                        <script>
                            function stop(){
                                $(".youtube-iframe")[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
                            }
                        </script>
                        <?php endif; ?>
                    </div>
                    <!-- <div class="embed-responsive embed-responsive-16by9"><?php //echo $video; ?></div> -->
                </div>
            </div>
        </div>
    </section>

    <section class="community-details">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <?php $c=0; if( have_rows('key_features') ): while( have_rows('key_features') ) : the_row(); $c++?>
                    <div class="key-features">
                        <div class="key-number">
                            <?php echo $c; ?>
                        </div>
                        <div class="key-detail">
                            <div class="key-title"><?php echo get_sub_field('feature');; ?></div>
                            <div class="key-description"><?php echo get_sub_field('description');; ?></div>
                        </div>
                    </div>
                    <?php endwhile; endif;?>
                </div>
                <div class="col-md-4">
                    <p><?php echo $tag_line; ?></p>
                </div>
            </div>
        </div>
    </section>

    <section class="community-content">
        <div class="container the-community">
            <div class="the-community-title">
                <h2 class="blue">The Community</h2>
            </div>
            <div class="the-community-menu">
                <nav class="nav">
                    <a class="nav-link blue" href="#overview">Overview</a>
                    <a class="nav-link blue" href="#location">Location</a>
                    <a class="nav-link blue" href="#properties">Properties</a>
                </nav>
            </div>
        </div>
        <div class="container community-grid">
            <?php if (have_rows('gallery_images')) :?>
               <?php while( have_rows('gallery_images') ): the_row(); ?>
               <div style="background:url(<?php echo get_sub_field('image'); ?>); background-size:cover;">
                    <!-- <img src="<?php //echo get_sub_field('image'); ?>" alt=""> -->
               </div>
                <?php endwhile;?>
            <?php endif;?>    
        </div>
        <div id="overview" class="container community-overview">
            <div class="row">
                <div class="col-md-12">
                    <small class="blue">Overview</small>
                    <h2 class="blue"><?php the_title();?></h2>
                    <p><?php echo $overview;?></p>
                </div>
            </div>
        </div>
        <div class="container community-overview_images">
            <?php if (have_rows('overview_images')) : $c = 0;?>
               <?php while( have_rows('overview_images') ): the_row(); $c++;
                $image_overview = get_sub_field('image'); ?>
               <?php if ($c == 1 || $c == 4 ):?>
                    <div class="main-img">
                        <img src="<?php echo wp_get_attachment_image_url($image_overview, 'slider-size'); ?>" alt="">
                    </div>
               <?php else:?>
               <div>
                    <img src="<?php echo wp_get_attachment_image_url($image_overview, 'square'); ?>" alt="">
               </div>
               <?php endif; endwhile;?>
            <?php endif;?>    
        </div>
        <div id="location" class="container community-location">
            <small class="blue">Location</small>
            <h3 class="blue"><?php echo $location_title;?></h3>
            <p><?php echo $location_ben;?></p>
            <div class="community-overview_images">
                <?php if (have_rows('location_images')) : $c = 0;?>
                <?php while( have_rows('location_images') ): the_row(); $c++;
                    $image_overview = get_sub_field('image'); ?>
                <?php if ($c == 1 || $c == 4 ):?>
                        <div class="main-img">
                            <img src="<?php echo wp_get_attachment_image_url($image_overview, 'slider-size'); ?>" alt="">
                        </div>
                <?php else:?>
                    <div>
                            <img src="<?php echo wp_get_attachment_image_url($image_overview, 'square'); ?>" alt="">
                    </div>
                <?php 
                    endif; 
                endwhile;
                endif;
                ?>  
            </div>
            <div class="community-map">
            <?php 
                $location = get_field('map');

                if( !empty($location) ):
                ?>
                <div class="acf-map">
                    <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                </div>
            <?php endif; ?>	
            </div>
        </div>
        <div id="properties" class="container community-properties">
            <small class="blue">Properties</small>
            <h3 class="blue">Investment oportunities in this location</h3>
            <p><?php echo $investment;?></p>
            
            <?php 
            // Query Arguments
            $location = $terms->slug;
            $args = array(
                'post_type' => array('buy','newdev','first-home'),
                'posts_per_page' => 4,
                'orderby' => 'rand',
                'location' => $location,
            );

            // The Query
            $Beach = new WP_Query( $args );
            ?>
            <div class="property-cards-container">
            <?php if ( $Beach->have_posts() ): while ( $Beach->have_posts() ): $Beach->the_post();?>
                <?php 
                    $propertyLink = get_post_permalink();
                    $propertyId =	get_the_ID();

                    $time = get_field('time');

                    //Get Firs tag
                    $allposttags = get_the_tags();
                    $i=0;
                    if ($allposttags) {
                        foreach($allposttags as $tags) {
                            $i++;
                            if (1 == $i) {
                                $firsttag = $tags->name;
                            }
                        }
                    }
                    //Get Location Slug
                    $terms = get_the_terms( $propertyId, 'location');
                    $count = count($terms);
                    if ( $count > 0 ){
                        foreach ( $terms as $term ) {
                        $locationGrid = $term->slug;

                        }
                    }
                ?>
                <!-- Price Card -->
                <div class="card grid-item grid-item--width2 transition <?php echo $locationGrid; ?>">
                    <a href="<?php echo $propertyLink; ?>">
                        <div class="card-img-top-container">
                            <img class="card-img-top" src="<?php the_post_thumbnail_url('feature'); ?>" alt="Card image cap">
                        </div>
                        <h5><span class="badge badge-primary"><?php echo $firsttag; ?></span></h5>
                        <div class="card-body">
                            <h4 class="card-title"><?php the_title(); ?></h4>
                            <h6 class="card-subtitle mb-2 text-muted location"><?php echo get_the_term_list( $propertyId, 'location'); ?></h6>
                            <p class="card-text">$<?php echo number_format(get_field('price'));?>
                                <?php if ( $time == 'Month' ): echo " /Month"; elseif ( $time == 'Night' ): echo " /Night"; elseif ( $time == 'Week' ): echo " /Week"; endif; ?>
                                <?php
                                $postObj = get_post_type_object( $the_post_type ); 
                                if ( $postObj->labels->singular_name == 'Lot' ): echo " /per square meter "; endif; ?>
                            </p>
                            <?php if($the_post_type !== "lot"): ?>
                            <span class="card-link disabled"><?php echo get_field('bedrooms')?> <i class="fa fa-bed" aria-hidden="true"></i></span>
                            <span class="card-link disabled"><?php echo get_field('bathrooms')?> <i class="fa fa-bath" aria-hidden="true"></i></span>
                            <span style="display:none;" class="number"><?php the_field('price'); ?></span>
                            <?php endif; ?>
                            <?php if(get_field('sale_status') == 'Sale Pending'): ?><span class="badge badge-warning pull-right">Sale Pending</span>
                            <?php elseif(get_field('sale_status') == 'Sold'): ?><span class="badge badge-success pull-right">Sold</span>
                            <?php endif; ?>
                        </div>
                    </a>
                </div>
                <!-- end price Card -->
            <?php endwhile; endif;
            /* Restore original Post Data */
            wp_reset_postdata();
            ?>
            </div>
        </div>
    </section>


<?php get_footer();?>