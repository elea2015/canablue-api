<?php /* Template Name: community*/ get_header(); ?>

    <section class="discoverHow homeSection">
		<div class="container text-center">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col">
					<h2><?php the_title();?></h2>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</section>

    <section class="community-items-card">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php 
                    // Query Arguments
                    $args = array(
                        'post_type' => array('community'),
                        'orderby' => 'date',
                        'order'   => 'ASC',
                    );

                    // The Query
                    $Beach = new WP_Query( $args );
                    $c=0;
                    ?>
                    <div class="property-cards-container">
                    <?php if ( $Beach->have_posts() ): while ( $Beach->have_posts() ): $Beach->the_post(); $c++ ?>
                        <?php 
                            $propertyLink = get_post_permalink();
                            $propertyId =	get_the_ID();

                            $time = get_field('time');

                            //Get Firs tag
                            $allposttags = get_the_tags();
                            $i=0;
                            if ($allposttags) {
                                foreach($allposttags as $tags) {
                                    $i++;
                                    if (1 == $i) {
                                        $firsttag = $tags->name;
                                    }
                                }
                            }
                            //Get Location Slug
                            $terms = get_the_terms( $propertyId, 'location');
                            $count = count($terms);
                            if ( $count > 0 ){
                                foreach ( $terms as $term ) {
                                $locationGrid = $term->slug;

                                }
                            }
                        ?>
                        <!-- Price Card -->
                        <div class="card grid-item <?php if($c <= 4){ echo 'grid-item--width2'; }else{ echo 'grid-item--width3'; }?> transition">
                            <div class="card-img-top-container">
                                <a href="<?php echo $propertyLink; ?>">
                                    <img class="card-img-top" src="<?php the_post_thumbnail_url('community'); ?>" alt="Card image cap">
                                </a>
                            </div>
                            <div class="card-body">
                                <a href="<?php echo $propertyLink; ?>"><h4 class="card-title"><?php the_title(); ?></h4></a>
                                <h6 class="card-subtitle mb-2 text-muted location"><?php echo get_the_term_list( $propertyId, 'location'); ?></h6>
                                <p class="card-text">
                                    <?php echo get_field('tag_line'); ?>
                                </p>
                                <a class="btn btn-primary" href="<?php echo $propertyLink; ?>">Learn more</a>
                            </div>
                        </div>
                        <!-- end price Card -->
                    <?php endwhile; endif;
                    /* Restore original Post Data */
                    wp_reset_postdata();
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>