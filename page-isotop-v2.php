<?php /* Template Name: isotop v2*/ get_header(); ?>

<?php
	//get page slug to define loop post type and content to show

    global $post;
    $post_slug=$post->post_name;

    if ($post_slug == 'new-construction' ):

	   $the_post_type = 'newdev';

	elseif($post_slug == 'economy'):

	   $the_post_type = 'first-home';

	elseif($post_slug == 'resale' || $post_slug == 'condos' || $post_slug == 'houses' || $post_slug == 'townhouses' || $post_slug == 'villas' || $post_slug == 'building'): 

	   $the_post_type = 'buy';

	elseif($post_slug == 'lots'):

	   $the_post_type = 'lot';

	elseif($post_slug == 'residential_rentals'):

		$the_post_type = 'rent';
		$post_slug = 'Residential';

	elseif($post_slug == 'commercial_rentals'):

		$the_post_type = 'rent';
		$post_slug = 'Commercial';
	
	elseif($post_slug == 'villas_rent'):

		$the_post_type = 'rent';
		$post_slug = 'villas';
	
	elseif($post_slug == 'condos_rent'):

		$the_post_type = 'rent';
		$post_slug = 'condos';

	endif;

 	//if ($post_slug == 'condos' ):

	//$the_post_type = 'newdev';

	//endif;
	$pageID = get_the_ID();
	if($pageID == 1300):
		$propertyType = 75;
	elseif($pageID == 685):
		$propertyType = 21;
	elseif($pageID == 1302):
		$propertyType = 80;
	elseif($pageID == 1306):
		$propertyType = 82;
	elseif($pageID == 1304):
		$propertyType = 20;
	//For rentals
	elseif($pageID == 19):
		$propertyType = 113;
	elseif($pageID == 1745):
		$propertyType = 114;
	endif;

?>
<script type="text/javascript">
	 (function ($, root, undefined) {
	
	$(function () {
		
		'use strict';
		$("#oftype").hide();
		$("#oftype").val("<?php echo $propertyType ?>");
		
	});
	
})(jQuery, this);
</script>
	<section class="buySection">
		<div class="container text-center">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col">
					<h2><?php the_title(); if($the_post_type !== "rent") : echo " For Sale"; elseif($the_post_type == "rent"): echo " For Rent"; endif; ?></h2>
					<div class="ownersSericeList"><?php the_field('pitch') ?></div>
					<div class="container searchForm"><?php echo do_shortcode( '[searchandfilter add_search_param="1" fields="location,type" hide_empty="1"  post_types="'.$the_post_type.'" submit_label="Search" all_items_labels="All Locations"]' ); ?></div>
					<div class="bd-example">
					<!-- /btn-group -->	
					  <?php if(is_user_logged_in()): ?>
					  <div id="priceGrid" class="btn-group button-group">
					    <div class="dropdown">
						  <button class="btn btn-light dropdown-toggle filtersGrid" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Price Range
						  </button>
						  <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
						  	<button class="dropdown-item button is-checked" data-price="*">show all range</button>
						    <button class="dropdown-item" data-price="numberGreaterThan50" type="button">50k or less</button>
						    <button class="dropdown-item" data-price="numberGreaterThan100" type="button">100k or less</button>
						    <button class="dropdown-item" data-price="numberGreaterThan150" type="button">150k or less</button>
						    <button class="dropdown-item" data-price="numberGreaterThan200" type="button">200k or less</button>
						  </div>
						</div>
					  </div>
					  <!-- /btn-group -->	
					  <div id="locationGrid" class="btn-group button-group">
					    <div class="dropdown">
						  <button class="btn btn-light dropdown-toggle filtersGrid" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Location
						  </button>
						  <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
						  	<button class="dropdown-item button is-checked" data-location="*">Show All Locations</button>
						    <button class="dropdown-item" data-location=".cortecito" type="button">Cortecito</button>
						    <button class="dropdown-item" data-location=".cocotal" type="button">Cocotal</button>
						    <button class="dropdown-item" data-location=".costa-bavaro" type="button">Costa Bavaro</button>
						    <button class="dropdown-item" data-location=".los-corales" type="button">Los Corales</button>
						  </div>
						</div>
					  </div>
					<?php endif; ?>
					</div>
					<!-- filter -->
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</section>
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	<section class="buySection buyListing">
		<div class="container">
		<?php
			$orig_query = $wp_query;

			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			//Query Arguments
			$args = array(
				'post_type' => $the_post_type,
				//'posts_per_page' => 18,
        		'paged' => $paged,
        		// 'orderby' => 'rand',
        		'tax_query' => array(
					array(
						'taxonomy' => 'type',
						'field' => 'name',
            			'terms' => array( $post_slug )
					),
				)
			);
			$wp_query = new WP_Query($args); 

		?>

		<?php if($wp_query->have_posts()) ?>
		   <div class="grid2">
		     <?php  while ( $wp_query->have_posts() ): $wp_query->the_post(); $a++;?>
		        	<?php 
		        		$propertyLink = get_post_permalink();
		        		$propertyId =	get_the_ID();

		        		$time = get_field('time');

		        		//Get Firs tag
		        		$allposttags = get_the_tags();
		                $i=0;
		                if ($allposttags) {
		                    foreach($allposttags as $tags) {
		                        $i++;
		                        if (1 == $i) {
		                            $firsttag = $tags->name;
		                        }
		                    }
		                }
		                //Get Location Slug
		                $terms = get_the_terms( $propertyId, 'location');
						 $count = count($terms);
						 if ( $count > 0 ){
						     foreach ( $terms as $term ) {
						       $locationGrid = $term->slug;

						     }
						 }
		        	 ?>
		          	<!-- Price Card -->
					<div class="card grid-item grid-item--width2 transition <?php echo $locationGrid; ?>">
						<a href="<?php echo $propertyLink; ?>">
							<div class="card-img-top-container">
								<img class="card-img-top" src="<?php the_post_thumbnail_url('cards'); ?>" alt="Card image cap">
							</div>
							<h5><span class="badge badge-primary"><?php echo $firsttag; ?></span></h5>
							<div class="card-body">
								<h4 class="card-title"><?php the_title(); ?></h4>
								<h6 class="card-subtitle mb-2 text-muted location"><?php echo get_the_term_list( $propertyId, 'location'); ?></h6>
								<p class="card-text">$<?php echo number_format(get_field('price'));?>
									<?php 
									if($the_post_type == 'rent'): 
										if ( $time == 'Month' ): echo " /Month"; elseif ( $time == 'Night' ): echo " /Night"; elseif ( $time == 'Week' ): echo " /Week"; endif;
									endif;
									?>
									<?php
									 $postObj = get_post_type_object( $the_post_type ); 
									if ( $postObj->labels->singular_name == 'Lot' ): echo " /per square meter "; endif; ?>
								</p>
								<?php if($the_post_type !== "lot"): ?>
								<span class="card-link disabled"><?php echo get_field('bedrooms')?> <i class="fa fa-bed" aria-hidden="true"></i></span>
								<span class="card-link disabled"><?php echo get_field('bathrooms')?> <i class="fa fa-bath" aria-hidden="true"></i></span>
								<span style="display:none;" class="number"><?php the_field('price'); ?></span>
								<?php endif; ?>
								<?php if(get_field('sale_status') == 'Sale Pending'): ?><span class="badge badge-warning pull-right">Sale Pending</span>
								<?php elseif(get_field('sale_status') == 'Sold'): ?><span class="badge badge-success pull-right">Sold</span>
								<?php endif; ?>
								<?php if(get_field('rent_status') == 'Rented'): ?><span class="badge badge-success pull-right">Rented</span><?php endif;?>
							</div>
						</a>
					</div>
					<!-- end price Card -->
		     <?php  //if($a % 3 === 0) :  echo '</div> <div class="row priceRow">'; endif; ?>
			<?php endwhile; ?>
			</div>

			<div class="nav-previous alignleft"><?php next_posts_link( 'Older posts' ); ?></div>
			<div class="nav-next alignright"><?php previous_posts_link( 'Newer posts' ); ?></div>
			<?php wp_reset_query(); ?> 
			</div>
		</div>
	</section>

	<?php endwhile; endif;  $wp_query = $orig_query; ?>

	<?php 
	get_template_part('include/optin');
	?>

	<?php get_template_part('include/zonas')?>

<?php get_footer(); ?>