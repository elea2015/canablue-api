<?php /* Template Name: Rentals*/ get_header(); ?>

<?php

//Get Genre Filters
// function get_genre_filters()
// {
//     $terms = get_terms('location');
//     $filters_html = false;
 
//     if( $terms ):
//         $filters_html = '<div class="btn-group"><button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Location</button><div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">';
 
//         foreach( $terms as $term )
//         {
//             $term_id = $term->term_id;
//             $term_name = $term->name;
 
//             $filters_html .= '<a class="dropdown-item term_id_'.$term_id.'" href="#">'.$term_name.'</a>';
//         }
//         $filters_html .= '</div></div>';
 
//         return $filters_html;
//     endif;
// }

?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<section class="buySection">
		<div class="container text-center">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<h2>Plan your next vacations or whatever</h2>
					<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec id elit non mi porta gravida at eget metus.</p>
					<br><br>
					<!-- Filter -->
					<div class="bd-example propertyFilters">
					  <?php //echo get_genre_filters(); ?>
					  </div><!-- /btn-group -->
					</div>
					<!-- filter -->
				</div>
			</div>
		</div>
	</section>

	<section class="buySection buyListing">
		<div class="container">
		<?php
			$orig_query = $wp_query;

			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			//Query Arguments
			$args = array(
				'post_type' => array('rent'),
				'posts_per_page' => 9,
        		'paged' => $paged,
			);
			$wp_query = new WP_Query($args); 

		?>

		<?php if($wp_query->have_posts()) ?>
		   <div class="row">
		     <?php  while ( $wp_query->have_posts() ): $wp_query->the_post(); $a++;?>
		        <div class="col-md-4">
		        	<?php 
		        		$propertyLink = get_post_permalink();
		        		$propertyId =	get_the_ID();

		        		$time = get_field('time');

		        		$allposttags = get_the_tags();
		                $i=0;
		                if ($allposttags) {
		                    foreach($allposttags as $tags) {
		                        $i++;
		                        if (1 == $i) {
		                            $firsttag = $tags->name;
		                        }
		                    }
		                }
		        	 ?>
		          	<!-- Price Card -->
					<div class="card">
						<a href="<?php echo $propertyLink; ?>">
							<div class="card-img-top-container">
								<img class="card-img-top" src="<?php the_post_thumbnail_url('custom-size'); ?>" alt="Card image cap">
							</div>
							<h5><span class="badge badge-primary"><?php echo $firsttag; ?></span></h5>
							<div class="card-body">
								<h4 class="card-title"><?php the_title(); ?></h4>
								<h6 class="card-subtitle mb-2 text-muted"><?php echo get_the_term_list( $propertyId, 'location'); ?></h6>
								<p class="card-text">$<?php echo number_format(get_field('price'));?><?php if ( $time == 'Month' ): echo " /Month"; elseif ( $time == 'Night' ): echo " /Night"; elseif ( $time == 'Week' ): echo " /Week"; endif; ?></p>
								<span class="card-link disabled"><?php echo get_field('bedrooms')?> <i class="fa fa-bed" aria-hidden="true"></i></span>
								<span class="card-link disabled"><?php echo get_field('bathrooms')?> <i class="fa fa-bath" aria-hidden="true"></i></span>
							</div>
						</a>
					</div>
					<!-- end price Card -->
		        </div>
		     <?php  if($a % 3 === 0) :  echo '</div> <div class="row priceRow">'; endif; ?>
			<?php endwhile; ?>

			<div class="nav-previous alignleft"><?php next_posts_link( 'Older posts' ); ?></div>
			<div class="nav-next alignright"><?php previous_posts_link( 'Newer posts' ); ?></div>
			<?php wp_reset_query(); ?> 
			</div>
		</div>
	</section>

<?php endwhile; endif;  $wp_query = $orig_query; ?>

	<?php get_template_part('include/optin'); ?>

	<?php get_template_part('include/zonas')?>

<?php get_footer(); ?>