<?php /* Template Name: Landing home 2020 */ get_header(); ?>

	<section class="hero heroHome heroSell heroVideo" style="background:url(<?php the_field('homeBg')?>);background-size:cover; background-position:center;">
		<video id="homeVideo" playsinline autoplay muted loop>
			<source src="<?php the_field('video')?>" type="video/mp4">
		</video>
		<div class="container">
			<div class="row">
				<div class="col">
					<h1><?php the_field('hero_heading') ?></h1>
					<p><?php the_field('discover_text') ?></p>
				</div>
			</div>
		</div>
		<img class="mouseIcon" width="40px" src="<?php echo get_template_directory_uri(); ?>/img/icon/mouse.png">
    </section>

    <?php get_template_part('include/optin'); ?>

    <section class="homePropertieCards pt-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 text-center">
                    <h3 class="blue">YOUR SOURCE OF REAL ESTATE INVESTMENTS AND CARIBBEAN LIVING IN PUNTA CANA</h2>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 homePropertieCard text-center">
                    <div class="p-4">
                        <img src="<?php echo get_template_directory_uri();?>/img/buy.svg" alt="">
                        <h5 class="blue">Buy a condo</h5>
                        <a href="/new-construction/" class="btn btn-primary">See our listings</a>
                    </div>
                </div>

                <div class="col-md-4 homePropertieCard text-center">
                    <div class="p-4">
                        <img src="<?php echo get_template_directory_uri();?>/img/book.svg" alt="">
                        <h5 class="blue">Book a virual tour</h5>
                        <a href="book-a-virtual-tour/" class="btn btn-primary">Book a tour</a>
                    </div>
                </div>

                <div class="col-md-4 homePropertieCard text-center">
                    <div class="p-4">
                        <img src="<?php echo get_template_directory_uri();?>/img/contact.svg" alt="">
                        <h5 class="blue">Contact us</h5>
                        <a href="/contact/" class="btn btn-primary">Send us a message</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php get_template_part('include/zonas')?>

    <section class="discoverHow homeSection">
		<div class="container text-center">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col">
					<h2><?php the_field('discover_heading');?></h2>
					<p><?php //the_field('discover_text');?></p>
					<a href="<?php echo site_url();?>/?page_id=10" class="btn btn-primary">Discover Now</a>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</section>

    <style type="text/css">
		#testimonials .row{
			justify-content:center;
		}
		.testimonial_rotator_star{
			color:#1b4073;
		}
		.testimonial_rotator_slide_title{
			font-size: 18px;
			color: #1b4073;
		}
	</style>
	<section id="testimonials" class="testimonials">
		<div class="container">
			<div class="row justify-contnent-center">
				<div class="col-md-10">
					<h1 class="mb-3 text-center blue">Testimonials</h1>
					<?php echo do_shortcode('[testimonial_rotator id=2834]'); ?>
				</div>
			</div>
		</div>
	</section>

    
<?php get_footer()?>