<?php /* Template Name: unit*/ get_header(); ?>

<section class="hero unitHero">
		<div class="opacityBg">
			<div class="container">
				<div class="row">
					<div class="col-8">
						<span>For Sale | Investment oportunitie </span>
						<h2>Cocotal 8</h2>
						<p><i class="fa fa-map-marker"></i> Cocotal Golf</p>
						<h4>$124,000</h4>
					</div>
					<div class="col-4">
						<div class="contactForm">
							<h5>Pamela Nuñez</h5>
							<p><i class="fa fa-mobile"></i> 809-975-3900 </p>
							<a href="#"><i class="fa fa-envelope-o"></i> pamela@canablue.com</a>
							<br><br>
							<!-- <form>
								<div class="form-group">
									<label for="exampleInputPassword1">First Name</label>
									<input type="Text" class="form-control" id="exampleInputPassword1" placeholder="First Name">
								</div>	
								<div class="form-group">
									<label for="exampleInputEmail1">Email address</label>
									<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
									<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
								</div>
								<div class="form-group">
									<label for="exampleInputPassword1">Phone Number</label>
									<input type="Text" class="form-control" id="exampleInputPassword1" placeholder="Phone Number">
								</div>
								<div class="form-group">
									<label for="exampleFormControlTextarea1">Message</label>
									<textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
								</div>
								<button type="submit" class="btn btn-primary btn-block">Submit</button>
							</form> -->
							<?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]') ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="unitDetails">
		<div class="container">
			<div class="row">
				<div class="col-8">
					<!-- gallery -->
					<div class="row gallery">
						<div class="col"><img src="<?php echo get_template_directory_uri(); ?>/img/buylisting.jpg" alt=""></div>
						<div class="col"><img src="<?php echo get_template_directory_uri(); ?>/img/buylisting.jpg" alt=""></div>
						<div class="col"><img src="<?php echo get_template_directory_uri(); ?>/img/buylisting.jpg" alt=""></div>
						<div class="col"><img src="<?php echo get_template_directory_uri(); ?>/img/buylisting.jpg" alt=""></div>
					</div>
					<br><br>
					<!-- Main info -->
					<div class="row mainInfo">
						<div class="col">
							<h3>Main Information</h3>
							<table class="table">
							  <!-- <thead>
							    <tr>
							      <th scope="col">#</th>
							      <th scope="col">First Name</th>
							      <th scope="col">Last Name</th>
							      <th scope="col">Username</th>
							    </tr>
							  </thead> -->
							  <tbody>
							    <tr>
							      <th>Listing Type</th>
							      <td>Apartament</td>
							      <th>Bedrooms</th>
							      <td>3</td>
							    </tr>
							    <tr>
							      <th>Square FT</th>
							      <td>1200</td>
							      <th>Year Built</th>
							      <td>2015</td>
							    </tr>
							    <tr>
							      <th>Square MT</th>
							      <td>350</td>
							      <th>Status</th>
							      <td>Ready</td>
							    </tr>
							    <tr>
							      <th>Lost Size MT</th>
							      <td>2000</td>
							      <th>Days on Market</th>
							      <td>400</td>
							    </tr>
							    <tr>
							      <th>BathRoom</th>
							      <td>2</td>
							    </tr>
							  </tbody>
							</table>
							<br><br>
							<div class="unitDescroption">
								<h3>Decription</h3>
								<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Maecenas faucibus mollis interdum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed posuere consectetur est at lobortis. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>
							</div>
						</div>
					</div>
					<br>
				</div>
			</div>
		</div>
	</section>
	<br>
	<br>
	<section class="unitMap">
		<div class="container">
			<div class="row">
				<div class="col">
					<h3>Point of Interest</h3>
				</div>
			</div>
		</div>
		<div class="map">
		</div>
	</section>

<?php get_footer(); ?>