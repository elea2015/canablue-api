
(function ($, root, undefined) {
	
	$(function () {
		
		'use strict';


    $( document ).ready(function() {
        //Gallery
        $('.owl-carousel').owlCarousel({
          margin:10,
          loop:true,
          items:4,
          nav:true
      });

        //add form classes to gravity
        $('input.medium').addClass('form-control');
        $('select.medium').addClass('form-control');
        $('textarea.medium').addClass('form-control');
        $('.gform_button').addClass('btn btn-primary');

        //add form seach class
        $('.postform').addClass('form-control');
        $('.searchandfilter input').addClass('btn btn-primary');

        //Modal trigger menu item
        $(".menu-item-116").click(function(){
            $("#contactModal").modal('show');
        });

        //filters hover
        $('.dropdown').hover(function() {
          $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
        }, function() {
          $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
        });

        // init Isotope
        var $grid = $('.grid2').isotope({
          itemSelector: '.grid-item',
          layoutMode: 'fitRows',
          fitRows: {gutter: 10},
          getSortData: {
            name: '.location',
            number: '.number parseInt',
          },
        });

        // filter functions
        var filterFns = {
          // show if number is greater than 50
          numberGreaterThan50: function() {
            var number = $(this).find('span.number').text();
            return parseInt( number ) < 50001;
          },
          // show if number is greater than 100
          numberGreaterThan100: function() {
            var number = $(this).find('span.number').text();
            return parseInt( number ) < 100001;
          },// show if number is greater than 150
          numberGreaterThan150: function() {
            var number = $(this).find('span.number').text();
            return parseInt( number ) < 150001;
          },
          // show if number is greater than 200
          numberGreaterThan200: function() {
            var number = $(this).find('span.number').text();
            return parseInt( number ) < 200000;
          },
          // show if name ends with -ium
          ium: function() {
            var name = $(this).find('.name').text();
            return name.match( /ium$/ );
          }
        };

        // bind filter button click
        $('#priceGrid').on( 'click', 'button', function() {
          var filterValue = $( this ).attr('data-price');
          // use filterFn if matches value
          filterValue = filterFns[ filterValue ] || filterValue;
          $grid.isotope({ filter: filterValue });
        });

        // bind filter button click
        $('#locationGrid').on( 'click', 'button', function() {
          var filterValue = $( this ).attr('data-location');
          // use filterFn if matches value
          filterValue = filterFns[ filterValue ] || filterValue;
          $grid.isotope({ filter: filterValue });
        });

        // bind sort button click
        $('#sorts').on( 'click', 'button', function() {
          var sortByValue = $(this).attr('data-sort-by');
          $grid.isotope({ sortBy: sortByValue });
        });

        // change is-checked class on buttons
        $('.button-group').each( function( i, buttonGroup ) {
          var $buttonGroup = $( buttonGroup );
          $buttonGroup.on( 'click', 'button', function() {
            $buttonGroup.find('.is-checked').removeClass('is-checked');
            $( this ).addClass('is-checked');
          });
        });

        $(".grid2").isotope( 'layout' );


        $(function () {
          $('[data-toggle="tooltip"]').tooltip()
        })

        //mobile button
      //   $(window).scroll(function(){
      //     $('#contactMobileButton').hide();
      //     if ($(window).width() <= 768){
      //       if ($(this).scrollTop() > 200) {
      //         $('#contactMobileButton').fadeIn(500);
      //       } else {
      //           $('#contactMobileButton').fadeOut(500);
      //       }
      //     }                          
        
      // });c

    });

	});
	
})(jQuery, this);
