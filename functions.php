<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: html5blank.com | @html5blank
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{ 
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 400, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');
    add_image_size( 'property-screenshot', 800, 500, array( 'left', 'top' ) );
    add_image_size('slider-size', 1200, 600, array( 'left', 'center' ));
    add_image_size('cards', 550, 300, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');
    add_image_size('feature', 330, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');
    add_image_size('community', 700, 500, true);
    add_image_size('square', 700, 700, true);

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // // Enables post and comment RSS feed links to head
    // add_theme_support('automatic-feed-links');

    // // Localisation Support
    // load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/


/*------------------------------------*\
	Custom Post Resale
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_html5()
{
    register_taxonomy_for_object_type('category', 'buy'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'buy');
    register_post_type('buy', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Resales', 'html5blank'), // Rename these to suit
            'singular_name' => __('Resale', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New Resale', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit Resale', 'html5blank'),
            'new_item' => __('New Resale', 'html5blank'),
            'view' => __('View Resale', 'html5blank'),
            'view_item' => __('View Resale', 'html5blank'),
            'search_items' => __('Search Resale', 'html5blank'),
            'not_found' => __('No Resales found', 'html5blank'),
            'not_found_in_trash' => __('No Resales found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'author'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        // 'rewrite' => array(
        //         'slug' => 'resale'
        // ),
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ), // Add Category and Post Tags support
        'show_in_rest' => true,
        'rest_base'    => 'buy',
        
    ));
}

/*------------------------------------*\
    Custom Post Rentals
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_resale()
{
    register_taxonomy_for_object_type('category', 'rent'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('location', 'rent'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'rent');
    register_post_type('rent', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Rentals', 'html5blank'), // Rename these to suit
            'singular_name' => __('Rental', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New Rental', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit Rental', 'html5blank'),
            'new_item' => __('New Rental', 'html5blank'),
            'view' => __('View Rental', 'html5blank'),
            'view_item' => __('View Rental', 'html5blank'),
            'search_items' => __('Search Rental', 'html5blank'),
            'not_found' => __('No Rentals found', 'html5blank'),
            'not_found_in_trash' => __('No Rentals found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'author'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ), // Add Category and Post Tags support
        'show_in_rest' => true,
        'rest_base'    => 'rent',
    ));
}

add_action('init', 'create_post_type_resale');


/*------------------------------------*\
    Custom Post New-Dev
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_new()
{
    register_taxonomy_for_object_type('category', 'newdev'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'newdev');
    register_post_type('newdev', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Developments', 'html5blank'), // Rename these to suit
            'singular_name' => __('Development', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New Development', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit Development', 'html5blank'),
            'new_item' => __('New Development', 'html5blank'),
            'view' => __('View Development', 'html5blank'),
            'view_item' => __('View Development', 'html5blank'),
            'search_items' => __('Search Development', 'html5blank'),
            'not_found' => __('No Developments found', 'html5blank'),
            'not_found_in_trash' => __('No Developments found in Trash', 'html5blank'),
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'author'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'rewrite' => array(
                'slug' => 'new'
        ),
        'taxonomies' => array(
            'post_tag',
            'category'
        ), // Add Category and Post Tags support
        'show_in_rest' => true,
        'rest_base'    => 'dev',
        'show_in_graphql' => true,
        'hierarchical' => true,
        'graphql_single_name' => 'dev',
        'graphql_plural_name' => 'devs',

    ));
}

add_action('init', 'create_post_type_new');


// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_first_home()
{
    register_taxonomy_for_object_type('category', 'newdev'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'newdev');
    register_post_type('first-home', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('First Homes', 'html5blank'), // Rename these to suit
            'singular_name' => __('First Home', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New First Home', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit First Home', 'html5blank'),
            'new_item' => __('New First Home', 'html5blank'),
            'view' => __('View First Home', 'html5blank'),
            'view_item' => __('View First Home', 'html5blank'),
            'search_items' => __('Search First Home', 'html5blank'),
            'not_found' => __('No First Homes found', 'html5blank'),
            'not_found_in_trash' => __('No First Homes found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'author'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ), // Add Category and Post Tags support'show_in_rest' => true,
        'show_in_rest' => true,
        'rest_base'    => 'new',
    ));
}

add_action('init', 'create_post_type_first_home');

// Create 1 Custom Post Lots
function create_post_type_lot()
{
    register_taxonomy_for_object_type('category', 'lot'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'lot');
    register_post_type('lot', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Lots', 'html5blank'), // Rename these to suit
            'singular_name' => __('Lot', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New Lot', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit Lot', 'html5blank'),
            'new_item' => __('New Lot', 'html5blank'),
            'view' => __('View Lot', 'html5blank'),
            'view_item' => __('View Lot', 'html5blank'),
            'search_items' => __('Search Lot', 'html5blank'),
            'not_found' => __('No Lots found', 'html5blank'),
            'not_found_in_trash' => __('No Lots found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'author'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}

add_action('init', 'create_post_type_lot');


// Create 1 Custom Post Lots
function create_post_type_community()
{
    register_taxonomy_for_object_type('category', 'community'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'community');
    register_post_type('community', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Communities', 'html5blank'), // Rename these to suit
            'singular_name' => __('Community', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New Community', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit Community', 'html5blank'),
            'new_item' => __('New Community', 'html5blank'),
            'view' => __('View Community', 'html5blank'),
            'view_item' => __('View Community', 'html5blank'),
            'search_items' => __('Search Community', 'html5blank'),
            'not_found' => __('No Communitys found', 'html5blank'),
            'not_found_in_trash' => __('No Communities found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail',
            'author'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        // 'taxonomies' => array(
        //     'post_tag',
        //     'category'
        // ) // Add Category and Post Tags support
        'show_in_rest' => true,
        'rest_base'    => 'community',
    ));
}

add_action('init', 'create_post_type_community');



// Register Taxonomy location
// Taxonomy Key: location
function create_location_tax() {

    $labels = array(
        'name'              => _x( 'locations', 'taxonomy general name', 'textdomain' ),
        'singular_name'     => _x( 'location', 'taxonomy singular name', 'textdomain' ),
        'search_items'      => __( 'Search locations', 'textdomain' ),
        'all_items'         => __( 'All locations', 'textdomain' ),
        'parent_item'       => __( 'Parent location', 'textdomain' ),
        'parent_item_colon' => __( 'Parent location:', 'textdomain' ),
        'edit_item'         => __( 'Edit location', 'textdomain' ),
        'update_item'       => __( 'Update location', 'textdomain' ),
        'add_new_item'      => __( 'Add New location', 'textdomain' ),
        'new_item_name'     => __( 'New location Name', 'textdomain' ),
        'menu_name'         => __( 'Locations', 'textdomain' ),
    );
    $args = array(
        'labels' => $labels,
        'description' => __( 'Property Neighborhood', 'textdomain' ),
        'hierarchical' => false,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => false,
        'show_tagcloud' => true,
        'show_in_quick_edit' => true,
        'show_admin_column' => true,
    );
    register_taxonomy( 'location', array('buy', 'rent', 'newdev', 'first-home', 'lot' ), $args );

}
add_action( 'init', 'create_location_tax' );

// Register Property Type
// Taxonomy Key: type
function create_type_tax() {

    $labels = array(
        'name'              => _x( 'types', 'taxonomy general name', 'textdomain' ),
        'singular_name'     => _x( 'type', 'taxonomy singular name', 'textdomain' ),
        'search_items'      => __( 'Search types', 'textdomain' ),
        'all_items'         => __( 'All types', 'textdomain' ),
        'parent_item'       => __( 'Parent type', 'textdomain' ),
        'parent_item_colon' => __( 'Parent type:', 'textdomain' ),
        'edit_item'         => __( 'Edit type', 'textdomain' ),
        'update_item'       => __( 'Update type', 'textdomain' ),
        'add_new_item'      => __( 'Add New type', 'textdomain' ),
        'new_item_name'     => __( 'New type Name', 'textdomain' ),
        'menu_name'         => __( 'Property type', 'textdomain' ),
    );
    $args = array(
        'labels' => $labels,
        'description' => __( 'Property Neighborhood', 'textdomain' ),
        'hierarchical' => false,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_rest' => false,
        'show_tagcloud' => true,
        'show_in_quick_edit' => true,
        'show_admin_column' => true,
    );
    register_taxonomy( 'type', array('buy', 'rent', 'newdev', 'first-home', 'lot' ), $args );

}
add_action( 'init', 'create_type_tax' );

function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyAijm0qExGIPfmZmCVtZi9oeNg7i0PxcK0');
}

add_action('acf/init', 'my_acf_init');

/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}

// Register Custom Navigation Walker
      require_once('bs4navwalker.php');


add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );
function my_show_extra_profile_fields( $user ) { ?>
<h3>Extra profile information</h3>
    <table class="form-table">
<tr>
            <th><label for="phone">Phone Number</label></th>
            <td>
            <input type="text" name="phone" id="phone" value="<?php echo esc_attr( get_the_author_meta( 'phone', $user->ID ) ); ?>" class="regular-text" /><br />
                <span class="description">Please enter your phone number.</span>
            </td>
</tr>
</table>
<?php }

add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );

function my_save_extra_profile_fields( $user_id ) {

if ( !current_user_can( 'edit_user', $user_id ) )
    return false;

update_usermeta( $user_id, 'phone', $_POST['phone'] );
}


//Filter by tax

function filter_cars_by_taxonomies( $post_type, $which ) {

    // Apply this only on a specific post type
    if ( 'buy' !== $post_type )
        return;

    // A list of taxonomy slugs to filter by
    $taxonomies = array( 'type', 'location' );

    foreach ( $taxonomies as $taxonomy_slug ) {

        // Retrieve taxonomy data
        $taxonomy_obj = get_taxonomy( $taxonomy_slug );
        $taxonomy_name = $taxonomy_obj->labels->name;

        // Retrieve taxonomy terms
        $terms = get_terms( $taxonomy_slug );

        // Display filter HTML
        echo "<select name='{$taxonomy_slug}' id='{$taxonomy_slug}' class='postform'>";
        echo '<option value="">' . sprintf( esc_html__( 'Show All %s', 'text_domain' ), $taxonomy_name ) . '</option>';
        foreach ( $terms as $term ) {
            printf(
                '<option value="%1$s" %2$s>%3$s (%4$s)</option>',
                $term->slug,
                ( ( isset( $_GET[$taxonomy_slug] ) && ( $_GET[$taxonomy_slug] == $term->slug ) ) ? ' selected="selected"' : '' ),
                $term->name,
                $term->count
            );
        }
        echo '</select>';
    }

}
add_action( 'restrict_manage_posts', 'filter_cars_by_taxonomies' , 10, 2);


function add_custom_types_to_tax( $query ) {
if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
 
// Get all your post types
$post_types =  array('post', 'buy', 'newdev', 'rent', 'lot');
 
$query->set( 'post_type', $post_types );
return $query;
}
}
add_filter( 'pre_get_posts', 'add_custom_types_to_tax' );

function get_excerpt(){
    $excerpt = get_the_content();
    $excerpt = preg_replace(" ([.*?])",'',$excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, 150);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace( '/s+/', ' ', $excerpt));
    //$excerpt = $excerpt.'... <a href="'.$permalink.'">more</a>';
    return $excerpt;
}

?>
