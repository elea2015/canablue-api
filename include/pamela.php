<section class="aboutPamela">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<h4>About Pamela</h4>
					<p>Born and raised in Santo Domingo, Dominican Republic, she started her career in real estate in Santo Domingo 10 years ago. Graduated from law school in the year 2010 from the Pontificia Universidad Catolica Madre y Maestra and once then, seeking greater business opportunities, Punta Cana called her attention in 2012.</p>
					<p>Punta Cana has been her home for the past 7 years working closely with foreign and domestic investors in the area. She is a Real Estate professional with a demonstrated history of working in the real estate and legal industry. She is skilled in Negotiation, Legal Affairs, Customer Acquisition, Customer Relationship Management, and Sales. Pamela has worked side by side with the top legal firms and real estate companies in the country, and was the commercial and sales director of one of the most successful real estate developments in Punta Cana.</p>
					<p>Partnering with her team, Pamela prides in being able to offer her clients a full cycle service range including real estate promotions and sales, real estate legal assistance, general legal assistance, property management and everything that CANABLUE has to offer in terms of construction, renovations and furnishing options.</p>
					<p>Pamela specializes in pre-construction real estate development, re-sales and legal matters in real estate transactions.</p>
				</div>
				<div class="col-md-4 wow fadeInRight">
					<img src="<?php echo get_template_directory_uri(); ?>/img/pamela.jpg">
				</div>
			</div>
		</div>
	</section>