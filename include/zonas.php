<section class="community-items-card">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="text-center blue">Neighborhoods and Communities</h2>
                <p class="px-5 mb-4 text-center blue">We are re-defining real estate in each area. Discover our hand-picked properties: From golf to beach views, including flourishing neighborhoods and ROI real estate opportunities. </p>
                <?php 
                // Query Arguments
                $args = array(
                    'post_type' => array('community'),
                    'orderby' => 'date',
                    'order'   => 'ASC',
                );

                // The Query
                $Beach = new WP_Query( $args );
                ?>
                <div class="property-cards-container">
                
                <?php 
                $c=0;
                if ( $Beach->have_posts() ): while ( $Beach->have_posts() ): $Beach->the_post(); $c++;?>
                    <?php 
                        $communityLink = get_post_permalink();
                        $communityId =	get_the_ID();

                        $time = get_field('time');

                        //Get Firs tag
                        $allposttags = get_the_tags();
                        $i=0;
                        if ($allposttags) {
                            foreach($allposttags as $tags) {
                                $i++;
                                if (1 == $i) {
                                    $firsttag = $tags->name;
                                }
                            }
                        }
                        //Get Location Slug
                        $terms = get_the_terms( $communityId, 'location');
                        $count = count($terms);
                        if ( $count > 0 ){
                            foreach ( $terms as $term ) {
                            $locationGrid = $term->slug;

                            }
                        }
                    ?>
                    <!-- Price Card -->
                    <div class="card grid-item <?php if($c <= 4){ echo 'grid-item--width2'; }else{ echo 'grid-item--width3'; }?> transition">
                        <div class="card-img-top-container">
                            <a href="<?php echo $communityLink; ?>">
                                <img class="card-img-top" src="<?php the_post_thumbnail_url('community'); ?>" alt="Card image cap">
                                <span><?php the_title(); ?></span>
                            </a>
                        </div>
                    </div>
                    <!-- end price Card -->
                <?php endwhile; endif;
                /* Restore original Post Data */
                wp_reset_postdata();
                ?>
                </div>
            </div>
        </div>
    </div>
</section>