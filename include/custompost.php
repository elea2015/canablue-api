<main role="main listing">
	<a id="contactMobileButton" class="btn btn-primary contactMobileButton" href="#contactForm">Contact Us</a>
	<span style="display:none"><?php echo $adwords;?></span>
	<!-- section -->
	<section>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php 
			//Get post data
			$location = 	get_the_term_list( $post->ID, 'location'); // location
			$price = 		number_format(get_field('price'));
			$listingType = 	get_field('listing_type');
			$squaterMT = 	number_format(get_field('square_mt'));
			$squareFT =		number_format(get_field('square_mt') * 10.764, 2, '.', '');//number_format(get_field('square_ft'));
			$lotSize = 		number_format(get_field('lot_size'));
			$bathrooms = 	get_field('bathrooms');
			$bedrooms = 	get_field('bedrooms');
			$status = 		get_field('status');
			$yearBuilt =	get_field('year_built');
			$taxes = 		get_field('taxes');
			$realTaxes =	get_field('realTaxes');
			$houseRules =	get_field('house_rules');
			$time =			get_field('time');
			$onMarket =		get_field('days_on_the_market');
			$confotur = 	get_field('confotur');
			$brochure =		get_field('brochure');
			$post_type =	get_post_type();
			$currentID = 	get_the_ID();
			$locationCom = 	get_the_terms( $post->ID, 'location' );
			$locationSlug = $locationCom[0]->slug;
			get_template_part('include/map');
			$virtual =		get_field('virtual_tour');
			?>


			<!-- Listing content -->
			<section id="unit-hero" class="listing-top-bar">
				<div class="container">
					<div class="d-flex listing-info-container">						
						<!-- Listing title and location	 -->
						<div class="listing-title">
							<h1><?php the_title(); ?></h1>
							<!-- Listing status -->
							<?php if(get_field('sale_status') == 'Sale Pending'): ?><span class="badge badge-warning">Sale Pending</span><br>
							<?php elseif(get_field('sale_status') == 'Sold'): ?><span class="badge badge-success">Sold</span><br>
							<?php elseif(get_field('sale_status') == 'Only several units left'): ?><span class="badge badge-warning">Only several units left</span><br>
							<?php elseif(get_field('sale_status') == 'Sold out'): ?><span class="badge badge-success">Sold out</span><br>
							<?php endif; ?>
							<?php if(get_field('rent_status') == 'Rented'): ?> <span class="badge badge-success">Rented</span><br><?php endif; ?>
							<!-- Listing location -->	
							<small class="listing-location"><i class="fa fa-map-marker"></i> <?php echo $location; ?> | </small>
							<!-- Listing type -->	
							<small><?php echo $listingType->name; ?></small>
						</div>
						<!-- Listing Pricing -->
						<div class="listing-details d-flex">
							<div class="listing-price">
								<?php 
								if (is_singular('newdev')){echo '<span class="blue">Prices starting at</span>';}
								if (is_singular('buy') or is_singular('lot')){echo '<span class="blue">Price</span>';}
								echo "<span>US$".$price . "</span>";
								if (is_singular( 'rent' )){
									if ( $time == 'Month' ) {echo " /mo";} 
									elseif ( $time == 'Night' ) {echo " /Night";} 
									elseif ( $time == 'Week' ) {echo " /Week";} 
									else {echo "/From";} 
								}
								if (is_singular('lot')){echo "/ per square meter ";}
								?>
							</div>
							<div class="listing-specs">
								<?php if(!is_singular('lot')):?>
									<div class="listing-beds">
										<span class="blue">Bedrooms</span>
										<span><?php echo $bedrooms; ?></span>
									</div>
									<div class="listing-bath">
										<span class="blue">Bathrooms</span>
										<span><?php echo $bathrooms; ?></span>
									</div>
								<?php endif; ?>
								<div class="listing-size">
									<span class="blue">Square FT</span>
									<span><?php echo $squareFT; ?></span>
								</div>
								<div class="listing-size">
									<span class="blue">Square MT</span>
									<span><?php echo $squaterMT; ?></span>
								</div>
							</div>
						</div>
						<!-- listing tags -->
						<div class="listing-tags">
							<?php
								$tags = $tags = wp_get_post_tags($post->ID);
								$tag_counter = 0;
								foreach($tags as $tag):
								$tag_counter++;
								if($tag_counter > 4){
									continue;
								}
								$tag_link = get_tag_link( $tag->term_id );
							?>
								<a class="badge badge-secondary" href="<?php echo $tag_link;?>"><?php echo $tag->name; ?></a>
							<?php endforeach;?>
						</div>
					</div>
				</div>
			</section>
			<!-- listing hero -->
			<section class="listing-hero">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							 <!--Carousel Wrapper-->
							<div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails unit-carousel" data-ride="carousel">
								<!--Slides-->
								<div class="carousel-inner" role="listbox">
									<?php $images = get_field('gallery'); if( $images ): $c = 0; ?>
										<?php foreach( $images as $image ): $c++; ?>
											<div class="carousel-item <?php if($c == 1) : echo "active"; endif;?>">
											<a href="<?php echo wp_get_attachment_url( $image['ID'] ); ?>" data-lightbox="image-1" >
												<img class="d-block w-100" src="<?php echo esc_url($image['sizes']['community']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
											</a>
											</div>
										<?php endforeach; ?>
									<?php endif; ?>
								</div>
								<!--/.Slides-->
								<!--Controls-->
								<a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
									<span class="carousel-control-prev-icon" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
								</a>
								<a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
									<span class="carousel-control-next-icon" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
								</a>
								<!--/.Controls-->
								<ol class="carousel-indicators">
									<?php if( $images ): $c = 0; ?>
										<?php foreach( $images as $image ): $c++; ?>
										<li data-target="#carousel-thumb" data-slide-to="<?php echo $c - 1; ?>" class="<?php if($c == 1) : echo "active"; endif;?>"> <img class="d-block w-100" src="<?php echo esc_url($image['sizes']['feature']); ?>"
										class="img-fluid">
										</li>
										<?php endforeach; ?>
									<?php endif; ?>
								</ol>
							</div>
							<!--/.Carousel Wrapper-->
							<!-- Unit details -->
							<div class="unitDetails">
								<div class="row">
									<div class="col-md-3">
										<nav class="nav flex-column property-nav">
											<h5>Navegation</h5>
											<?php if($virtual):?>
												<li><a class="nav-link blue" href="#virtual">Virtual Tour</a></li>
											<?php endif;?>
											<li><a class="nav-link blue" href="#unit-hero">Overview</a></li>
											<li><a class="nav-link blue" href="#unit-description">Description</a></li>
											<li><a class="nav-link blue" href="#unit-location">Location</a></li>
											<li><a class="nav-link blue" href="#unit-agent">Agent</a></li>
											<?php if($locationSlug == 'cap-cana' 
											or $locationSlug == 'el-cortecito' 
											or $locationSlug == 'los-corales' 
											or $locationSlug == 'palma-real-villas-cocotal' 
											or $locationSlug == 'punta-cana'
											or $locationSlug == 'cana-bay'
											or $locationSlug == 'downtown-punta-cana'
											or $locationSlug == 'white-sands'
											or $locationSlug == 'vista-cana'): ?>
											<li ><a class="nav-link blue" href="#unit-community">Community</a></li>
											<?php endif;?>
											<li><a class="nav-link blue" href="#unit-related">Related</a></li>
										</nav>
									</div>
									<div class="col-md-9">
										<?php if($brochure): ?>
										<div class="brochure">
											<?php echo do_shortcode('[et_bloom_locked optin_id="optin_1"]<a href="'.$brochure.'" target="_blank" class="btn btn-primary">Download Brochure</a>[/et_bloom_locked]') ?>
										</div>
										<?php endif; ?>
										<?php if($confotur):?>
										<div class="row">
											<div class="col">
												<style type="text/css">
												.tooltip-inner{
													min-width: 370px;
												}
												</style>
												<h5>Confotur (Tax Free) <i class="fa fa-question-circle" data-toggle="tooltip" data-html="true" title="<p class='text-left'>CONFOTUR is a tourism incentive law in the Dominican Republic with the sole purpose to increase foreign investment in touristic areas such as Punta Cana. For buyers and investors in Punta Cana, purchasing a condo with CONFOTUR means being free of transfer tax (3%) and from property tax (1%) for 15 years.</p>"></i></h5>
											</div>
										</div>
										<?php endif?>
									
										<!-- Main info -->
										<div id="unit-description" class="row mainInfo">
											<div class="col">
												<div class="unitDescroption">
												<?php if($virtual):?>
													<h5 class="blue" id="virtual">Virtual Tour</h5>
													<div class="embed-responsive embed-responsive-16by9 mb-4">
														<?php echo $virtual; ?>
													</div>
												<?php endif;?>
												<h5 class="blue">Description</h5>
													<?php the_content(); ?>
												</div>
												
												<div class="unitDescroption">
													<?php if($houseRules): ?>
														<h5 class="blue mb-2">House Rules</h5>
													<?php 
														echo $houseRules;
														endif;
													?>
												</div>
	
												<div id="unit-location" class="unitLocation mt-5">
													<h5 class="blue mb-2">Location</h5>
													<?php 
														$location_map = get_field('map');
	
														if( !empty($location_map) ):
														?>
														<div class="acf-map">
															<div class="marker" data-lat="<?php echo $location_map['lat']; ?>" data-lng="<?php echo $location_map['lng']; ?>"></div>
														</div>
													<?php endif; ?>	
												</div>
	
												<div id="unit-agent" class="unitAgent mt-5">
													<h5 class="blue mb-2">Agent</h5>
													<div class="listing-form-header">
														<div class="listing-form-agent-picture"><?php echo get_avatar( get_the_author_meta( 'ID' ),200); ?><br></div>
														<div class="listing-form-agent-details">
															<h5><?php the_author_meta( 'first_name'); ?> <?php the_author_meta( 'last_name'); ?></h5>
															<p><i class="fa fa-mobile"></i> +1-<?php the_author_meta( 'phone'); ?> </p>
															<a href="#"><i class="fa fa-envelope-o"></i> <?php the_author_meta( 'user_email'); ?></a>
														</div>
													</div>
													<div class="mt-3">
														<?php echo wpautop( get_the_author_meta( 'description' ) );?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- Unit details -->
						</div>
						<div id="contactForm" class="col-md-4">
							<div class="contactForm">
								<div class="listing-form-header">
									<div class="listing-form-agent-picture"><?php echo get_avatar( get_the_author_meta( 'ID' ),200); ?><br></div>
									<div class="listing-form-agent-details">
										<h5><?php the_author_meta( 'first_name'); ?> <?php the_author_meta( 'last_name'); ?></h5>
										<p><i class="fa fa-mobile"></i> +1-<?php the_author_meta( 'phone'); ?> </p>
										<a href="#"><i class="fa fa-envelope-o"></i> <?php the_author_meta( 'user_email'); ?></a>
									</div>
								</div>
								<?php
									$property_field = get_the_title();
									$author_id=$post->post_author;
									$agent_email = get_the_author_meta( 'user_email', $author_id );
									$agent_name = get_the_author_meta( 'display_name', $author_id );
									$propertyForm = do_shortcode('[gravityform id="2" field_values="property='.$property_field.'&agent='.$agent_email.'&agent_name='.$agent_name.'" title="false" description="false"]');
									echo $propertyForm;
								?>
							</div>
						</div>
					</div>
				</div>
			</section>

			
			<section class="container">
				<!-- community Loop -->
				<?php if($locationSlug == 'cap-cana' 
				or $locationSlug == 'el-cortecito' 
				or $locationSlug == 'los-corales' 
				or $locationSlug == 'palma-real-villas-cocotal'
				or $locationSlug == 'costa-bavaro'
				or $locationSlug == 'veron'   
				or $locationSlug == 'punta-cana'
				or $locationSlug == 'cana-bay'
				or $locationSlug == 'downtown-punta-cana'
				or $locationSlug == 'white-sands'
				or $locationSlug == 'vista-cana'): ?>
				<div id="unit-community" class="unitCommunity">
					<div class="row">
						<div class="col-md-6">
							<h5 class="blue mt-5">Community</h5>
						</div>
					</div>
					<?php
						if($locationSlug == 'cap-cana'){
							$communityID = 11698;
						}elseif($locationSlug == 'el-cortecito' or $locationSlug == 'los-corales'){
							$communityID = 11697;
						}
						elseif($locationSlug == 'palma-real-villas-cocotal'){
							$communityID = 7628;
						}elseif($locationSlug == 'punta-cana'){
							$communityID = 11696;
						}elseif($locationSlug == 'costa-bavaro'){
							$communityID = 12033;
						}elseif($locationSlug == 'veron'){
							$communityID = 12034;
						}elseif($locationSlug == 'cana-bay'){
							$communityID = 12197;
						}elseif($locationSlug == 'downtown-punta-cana'){
							$communityID = 12198;
						}elseif($locationSlug == 'white-sands'){
							$communityID = 12244;
						}elseif($locationSlug == 'vista-cana'){
							$communityID = 12196;
						}
						$args_query = array(
							'post_type' => array('community'),
							'order' => 'DESC',
							'p' => $communityID,
						);
		
						$query = new WP_Query( $args_query );
		
						if ($query->have_posts()) : while ( $query->have_posts() ) : $query->the_post(); 
						$community_description = get_field('overview');
						?>
							<div class="row">
								<div class="col-md-8 unitCommunityVideoContainer">
									<img class="listing-image mb-4" src="<?php the_post_thumbnail_url('community');  ?>">
									<?php if(get_field('video')):?>
									<a class="unitCommunityVideo" href="#" data-toggle="modal" data-target="#videoModal">
										<img src="<?php echo get_template_directory_uri(); ?>/img/icon/play-button.svg" alt="">
									</a>
									<!-- modal -->
									<div class="modal fade" id="videoModal" tabindex="-1" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title blue"><?php the_title(); ?></h5>
													<button onclick="stop()" type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body embed-responsive embed-responsive-16by9">
													<?php the_field('video'); ?>
												</div>
											</div>
										</div>
									</div>
									<!-- modal -->
									<script>
										function stop(){
											$(".youtube-iframe")[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
										}
									</script>
									<?php endif;?>
								</div>
								<div class="col-md-4">
									<a href="<?php echo get_permalink(); ?>"><h3 class="blue"><?php the_title(); ?></h3></a>
									<p><?php echo $community_description?></p>
									<a href="<?php echo get_permalink(); ?>">More about the community</a>
								</div>
							</div>
						<?php
						endwhile;
						endif;
						wp_reset_postdata();
					?>
				</div>
					<?php endif;?>
				<!-- related -->
				<div id="unit-related" class="unitRelated mt-5">
					<div class="row">
						<div class="col-md-12">
							<h5 class="blue">Similar properties available</h5>
							<?php
							$args_query = array(
								'post_type' => $post_type,
								'posts_per_page' => 4,
								'orderby' => 'rand',
								'post__not_in' => array($currentID),
								'tax_query' => array(
									array(
										'taxonomy' => 'location',
										'field' => 'slug',
										'terms' => $locationSlug
									),
								)
							);
			
							$query = new WP_Query( $args_query );
			
							
							?>
							<div class="property-cards-container">
								
								<?php 
								if ($query->have_posts()) : while ( $query->have_posts() ) : $query->the_post();
									$propertyLink = get_post_permalink();
									$propertyId =	get_the_ID();

									$time = get_field('time');

									//Get Firs tag
									$allposttags = get_the_tags();
									$i=0;
									if ($allposttags) {
										foreach($allposttags as $tags) {
											$i++;
											if (1 == $i) {
												$firsttag = $tags->name;
											}
										}
									}
									//Get Location Slug
									$terms = get_the_terms( $propertyId, 'location');
									$count = count($terms);
									if ( $count > 0 ){
										foreach ( $terms as $term ) {
										$locationGrid = $term->slug;

										}
									}
								?>
								<!-- Price Card -->
								<div class="card grid-item grid-item--width2 transition <?php echo $locationGrid; ?>">
									<a href="<?php echo $propertyLink; ?>">
										<div class="card-img-top-container">
											<img class="card-img-top" src="<?php the_post_thumbnail_url('feature'); ?>" alt="Card image cap">
										</div>
										<h5><span class="badge badge-primary"><?php echo $firsttag; ?></span></h5>
										<div class="card-body">
											<h4 class="card-title"><?php the_title(); ?></h4>
											<h6 class="card-subtitle mb-2 text-muted location"><?php echo get_the_term_list( $propertyId, 'location'); ?></h6>
											<p class="card-text">$<?php echo number_format(get_field('price'));?>
												<?php if ( $time == 'Month' ): echo " /Month"; elseif ( $time == 'Night' ): echo " /Night"; elseif ( $time == 'Week' ): echo " /Week"; endif; ?>
												<?php
												$postObj = get_post_type_object( $the_post_type ); 
												if ( $postObj->labels->singular_name == 'Lot' ): echo " /per square meter "; endif; ?>
											</p>
											<?php if($the_post_type !== "lot"): ?>
											<span class="card-link disabled"><?php echo get_field('bedrooms')?> <i class="fa fa-bed" aria-hidden="true"></i></span>
											<span class="card-link disabled"><?php echo get_field('bathrooms')?> <i class="fa fa-bath" aria-hidden="true"></i></span>
											<span style="display:none;" class="number"><?php the_field('price'); ?></span>
											<?php endif; ?>
											<?php if(get_field('sale_status') == 'Sale Pending'): ?><span class="badge badge-warning pull-right">Sale Pending</span>
											<?php elseif(get_field('sale_status') == 'Sold'): ?><span class="badge badge-success pull-right">Sold</span>
											<?php endif; ?>
										</div>
									</a>
								</div>
								<!-- end price Card -->
							<?php endwhile; endif;
							/* Restore original Post Data */
							wp_reset_postdata();
							?>
							</div>
						</div>
					</div>
				</div>
			</section>
		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php endif; ?>

	</section>
	<!-- /section -->
	</main>
	<!-- $('#videoModal > div > div > div.modal-body.embed-responsive.embed-responsive-16by9 > p > iframe')[0].src += "&autoplay=1"; -->