<section class="optin"> <!-- wow fadeInUp -->
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h5>Keep up to date for awesome deals!</h5>
					<p>Explore Punta Cana’s amazing real estate deals</p>
				</div>
				<div class="col-md-6">
            <div class="">
              <?php echo do_shortcode('[gravityform id="7" title="false" description="false" ajax="true"]') ?>
             </div> 
        </div>
			</div>
		</div>
	</section>

  <style type="text/css">
    #gform_wrapper_7{margin:0;}
    #gform_7{display:flex; padding:0;}
    #field_7_1{padding: 0;}
    #field_7_1 .gfield_label{display:none}
    #input_7_1 {width: 100%;}
    #gform_wrapper_7 .gform_footer{margin:0;}
    #gform_confirmation_message_7{color:white;}
    #gform_wrapper_7 .validation_error{display:none}
    @media(max-width: 768px){
      #gform_7{
        flex-direction:column;
      }
    }
  </style>