<div class="row">
	<div class="col-12">
		<h2 class="blue text-center py-3">Beach Proximity properties</h2>
	</div>
</div>


<?php 
// Query Arguments
$args = array(
	'post_type' => array('buy','newdev'),
	'posts_per_page' => 3,
	'orderby' => 'rand',
	'category_name' => 'Beach Proximity'
);

// The Query
$Beach = new WP_Query( $args );
?>
<div class="grid2">
<?php if ( $Beach->have_posts() ): while ( $Beach->have_posts() ): $Beach->the_post();?>
	<?php 
		$propertyLink = get_post_permalink();
		$propertyId =	get_the_ID();

		$time = get_field('time');

		//Get Firs tag
		$allposttags = get_the_tags();
        $i=0;
        if ($allposttags) {
            foreach($allposttags as $tags) {
                $i++;
                if (1 == $i) {
                    $firsttag = $tags->name;
                }
            }
        }
        //Get Location Slug
        $terms = get_the_terms( $propertyId, 'location');
		 $count = count($terms);
		 if ( $count > 0 ){
		     foreach ( $terms as $term ) {
		       $locationGrid = $term->slug;

		     }
		 }
	 ?>
  	<!-- Price Card -->
	<div class="card grid-item grid-item--width2 transition <?php echo $locationGrid; ?>">
		<a href="<?php echo $propertyLink; ?>">
			<div class="card-img-top-container">
				<img class="card-img-top" src="<?php the_post_thumbnail_url('feature'); ?>" alt="Card image cap">
			</div>
			<h5><span class="badge badge-primary"><?php echo $firsttag; ?></span></h5>
			<div class="card-body">
				<h4 class="card-title"><?php the_title(); ?></h4>
				<h6 class="card-subtitle mb-2 text-muted location"><?php echo get_the_term_list( $propertyId, 'location'); ?></h6>
				<p class="card-text">$<?php echo number_format(get_field('price'));?>
					<?php if ( $time == 'Month' ): echo " /Month"; elseif ( $time == 'Night' ): echo " /Night"; elseif ( $time == 'Week' ): echo " /Week"; endif; ?>
					<?php
					 $postObj = get_post_type_object( $the_post_type ); 
					if ( $postObj->labels->singular_name == 'Lot' ): echo " /per square meter "; endif; ?>
				</p>
				<?php if($the_post_type !== "lot"): ?>
				<span class="card-link disabled"><?php echo get_field('bedrooms')?> <i class="fa fa-bed" aria-hidden="true"></i></span>
				<span class="card-link disabled"><?php echo get_field('bathrooms')?> <i class="fa fa-bath" aria-hidden="true"></i></span>
				<span style="display:none;" class="number"><?php the_field('price'); ?></span>
				<?php endif; ?>
				<?php if(get_field('sale_status') == 'Sale Pending'): ?><span class="badge badge-warning pull-right">Sale Pending</span>
				<?php elseif(get_field('sale_status') == 'Sold'): ?><span class="badge badge-success pull-right">Sold</span>
				<?php endif; ?>
			</div>
		</a>
	</div>
	<!-- end price Card -->
<?php endwhile; endif;
/* Restore original Post Data */
wp_reset_postdata();
?>
</div>


<div class="row">
	<div class="col-12">
		<h2 class="blue text-center py-3">Golf Properties</h2>
	</div>
</div>


<?php 
// Query Arguments
$args = array(
	'post_type' => array('buy','newdev'),
	'posts_per_page' => 3,
	'orderby' => 'rand',
	'category_name' => 'Golf Property'
);

// The Query
$Beach = new WP_Query( $args );
?>
<div class="grid2">
<?php if ( $Beach->have_posts() ): while ( $Beach->have_posts() ): $Beach->the_post();?>
	<?php 
		$propertyLink = get_post_permalink();
		$propertyId =	get_the_ID();

		$time = get_field('time');

		//Get Firs tag
		$allposttags = get_the_tags();
        $i=0;
        if ($allposttags) {
            foreach($allposttags as $tags) {
                $i++;
                if (1 == $i) {
                    $firsttag = $tags->name;
                }
            }
        }
        //Get Location Slug
        $terms = get_the_terms( $propertyId, 'location');
		 $count = count($terms);
		 if ( $count > 0 ){
		     foreach ( $terms as $term ) {
		       $locationGrid = $term->slug;

		     }
		 }
	 ?>
  	<!-- Price Card -->
	<div class="card grid-item grid-item--width2 transition <?php echo $locationGrid; ?>">
		<a href="<?php echo $propertyLink; ?>">
			<div class="card-img-top-container">
				<img class="card-img-top" src="<?php the_post_thumbnail_url('feature'); ?>" alt="Card image cap">
			</div>
			<h5><span class="badge badge-primary"><?php echo $firsttag; ?></span></h5>
			<div class="card-body">
				<h4 class="card-title"><?php the_title(); ?></h4>
				<h6 class="card-subtitle mb-2 text-muted location"><?php echo get_the_term_list( $propertyId, 'location'); ?></h6>
				<p class="card-text">$<?php echo number_format(get_field('price'));?>
					<?php if ( $time == 'Month' ): echo " /Month"; elseif ( $time == 'Night' ): echo " /Night"; elseif ( $time == 'Week' ): echo " /Week"; endif; ?>
					<?php
					 $postObj = get_post_type_object( $the_post_type ); 
					if ( $postObj->labels->singular_name == 'Lot' ): echo " /per square meter "; endif; ?>
				</p>
				<?php if($the_post_type !== "lot"): ?>
				<span class="card-link disabled"><?php echo get_field('bedrooms')?> <i class="fa fa-bed" aria-hidden="true"></i></span>
				<span class="card-link disabled"><?php echo get_field('bathrooms')?> <i class="fa fa-bath" aria-hidden="true"></i></span>
				<span style="display:none;" class="number"><?php the_field('price'); ?></span>
				<?php endif; ?>
				<?php if(get_field('sale_status') == 'Sale Pending'): ?><span class="badge badge-warning pull-right">Sale Pending</span>
				<?php elseif(get_field('sale_status') == 'Sold'): ?><span class="badge badge-success pull-right">Sold</span>
				<?php endif; ?>	
			</div>
		</a>
	</div>
	<!-- end price Card -->
<?php endwhile; endif;
/* Restore original Post Data */
wp_reset_postdata();
?>
</div>

<div class="row">
	<div class="col-12">
		<h2 class="blue text-center py-3">Economy properties</h2>
	</div>
</div>


<?php 
// Query Arguments
$args = array(
	'post_type' => array('buy','newdev'),
	'posts_per_page' => 3,
	'orderby' => 'rand',
	'category_name' => 'Economy Property'
);

// The Query
$Beach = new WP_Query( $args );
?>
<div class="grid2">
<?php if ( $Beach->have_posts() ): while ( $Beach->have_posts() ): $Beach->the_post();?>
	<?php 
		$propertyLink = get_post_permalink();
		$propertyId =	get_the_ID();

		$time = get_field('time');

		//Get Firs tag
		$allposttags = get_the_tags();
        $i=0;
        if ($allposttags) {
            foreach($allposttags as $tags) {
                $i++;
                if (1 == $i) {
                    $firsttag = $tags->name;
                }
            }
        }
        //Get Location Slug
        $terms = get_the_terms( $propertyId, 'location');
		 $count = count($terms);
		 if ( $count > 0 ){
		     foreach ( $terms as $term ) {
		       $locationGrid = $term->slug;

		     }
		 }
	 ?>
  	<!-- Price Card -->
	<div class="card grid-item grid-item--width2 transition <?php echo $locationGrid; ?>">
		<a href="<?php echo $propertyLink; ?>">
			<div class="card-img-top-container">
				<img class="card-img-top" src="<?php the_post_thumbnail_url('feature'); ?>" alt="Card image cap">
			</div>
			<h5><span class="badge badge-primary"><?php echo $firsttag; ?></span></h5>
			<div class="card-body">
				<h4 class="card-title"><?php the_title(); ?></h4>
				<h6 class="card-subtitle mb-2 text-muted location"><?php echo get_the_term_list( $propertyId, 'location'); ?></h6>
				<p class="card-text">$<?php echo number_format(get_field('price'));?>
					<?php if ( $time == 'Month' ): echo " /Month"; elseif ( $time == 'Night' ): echo " /Night"; elseif ( $time == 'Week' ): echo " /Week"; endif; ?>
					<?php
					 $postObj = get_post_type_object( $the_post_type ); 
					if ( $postObj->labels->singular_name == 'Lot' ): echo " /per square meter "; endif; ?>
				</p>
				<?php if($the_post_type !== "lot"): ?>
				<span class="card-link disabled"><?php echo get_field('bedrooms')?> <i class="fa fa-bed" aria-hidden="true"></i></span>
				<span class="card-link disabled"><?php echo get_field('bathrooms')?> <i class="fa fa-bath" aria-hidden="true"></i></span>
				<span style="display:none;" class="number"><?php the_field('price'); ?></span>
				<?php endif; ?>
				<?php if(get_field('sale_status') == 'Sale Pending'): ?><span class="badge badge-warning pull-right">Sale Pending</span>
				<?php elseif(get_field('sale_status') == 'Sold'): ?><span class="badge badge-success pull-right">Sold</span>
				<?php endif; ?>
			</div>
		</a>
	</div>
	<!-- end price Card -->
<?php endwhile; endif;
/* Restore original Post Data */
wp_reset_postdata();
?>
</div>