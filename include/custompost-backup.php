<main role="main listing">
	<!-- section -->
	<section>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php 
			//Get post data
			$location = 	get_the_term_list( $post->ID, 'location'); // location
			$price = 		number_format(get_field('price'));
			$listingType = 	get_field('listing_type');
			$squareFT =		number_format(get_field('square_ft'));
			$squaterMT = 	number_format(get_field('square_mt'));
			$lotSize = 		number_format(get_field('lot_size'));
			$bathrooms = 	number_format(get_field('bathrooms'));
			$bedrooms = 	number_format(get_field('bedrooms'));
			$status = 		get_field('status');
			$yearBuilt =	get_field('year_built');
			$taxes = 		get_field('taxes');
			$houseRules =	get_field('house_rules');
			$time =			get_field('time');
			?>


			<!-- Listing content -->
			<section class="hero unitHero" style="background: url('<?php the_post_thumbnail_url();  ?>'); background-size:cover;">
				<div class="opacityBg">
					<div class="container">
						<div class="row">
							<div class="col-md-9">
								<span><?php the_tags( __( '', 'html5blank' ), ' | '); ?></span>
								<h2><?php the_title(); ?></h2>
								<p><i class="fa fa-map-marker"></i> <?php echo $location; ?></p>
								<h4>
									$<?php echo $price; ?>
									<?php 
										if ( $time == 'Month' ) {echo " /Month";} 
										elseif ( $time == 'Night' ) {echo " /Night";} 
										elseif ( $time == 'Week' ) {echo " /Week";} 
										else {echo "/From";} ?>
								</h4>
							</div>
							<div class="col-md-3">
								<div class="contactForm">
									<?php echo get_avatar( get_the_author_meta( 'ID' ),200); ?><br>
									<h5><?php the_author_meta( 'first_name'); ?> <?php the_author_meta( 'last_name'); ?></h5>
									<p><i class="fa fa-mobile"></i> +1-<?php the_author_meta( 'phone'); ?> </p>
									<a href="#"><i class="fa fa-envelope-o"></i> <?php the_author_meta( 'user_email'); ?></a>
									<?php
									$property_field = get_the_title();
									echo do_shortcode('[gravityform id="2" field_values="property='.$property_field.'" title="false" description="false"]');
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section> 

			<section class="unitDetails">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<div class="gallery">
							<!-- gallery -->
							<?php 
							$images = get_field('gallery');
							$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
							?>
							<div class="owl-carousel owl-theme">
								<?php if( $images ): ?>
							        <?php foreach( $images as $image ): ?>
							            <div class="item">
							            	<a href="<?php echo wp_get_attachment_url( $image['ID'] ); ?>" data-lightbox="image-1" data-title="<?php echo get_the_title($image['ID'] ) ?>">
							            		<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
							            	</a>
							            </div>
							        <?php endforeach; ?>
								<?php endif; ?>
							</div>
							</div>
							<br><br>
							<div class="row saleBenefits">
								<div class="col-md-6">
									<h4 class="taxes">
										<?php if( $taxes): ?>
										<?php echo $taxes;?>% Tax
										<?php endif; ?>
									</h4>
								</div>
								<div class="col-md-6">
									<h4 class="taxes">
										<?php if( $taxes): ?>
										<?php echo $taxes;?>% Real Estate tax (IPI)
										<?php endif; ?>
									</h4>
								</div>
							</div>
							<br><br>
							<!-- Main info -->
							<div class="row mainInfo">
								<div class="col">
									<h3>Main Information</h3>
									<table class="table">
									  <!-- <thead>
									    <tr>
									      <th scope="col">#</th>
									      <th scope="col">First Name</th>
									      <th scope="col">Last Name</th>
									      <th scope="col">Username</th>
									    </tr>
									  </thead> -->
									  <tbody>
									    <tr>
									      <th>Listing Type</th>
									      <td><?php echo $listingType; ?></td>
									      <th>Bedrooms</th>
									      <td><?php echo $bedrooms; ?></td>
									    </tr>
									    <tr>
									      <th>Square FT</th>
									      <td><?php echo $squareFT; ?></td>
									      <th>Year Built</th>
									      <td><?php echo $yearBuilt; ?></td>
									    </tr>
									    <tr>
									      <th>Square MT</th>
									      <td><?php echo $squaterMT; ?></td>
									      <th>Status</th>
									      <td><?php echo $status; ?></td>
									    </tr>
									    <tr>
									      <th>Lot Size MT</th>
									      <td><?php echo $lotSize; ?></td>
									    </tr>
									    <tr>
									      <th>BathRoom</th>
									      <td><?php echo $bathrooms; ?></td>
									    </tr>
									  </tbody>
									</table>
									<br><br>
									<div class="unitDescroption">
										<h3>Decription</h3>
										<?php the_content(); // Dynamic Content ?>
									</div>
									<br>
									<div class="unitDescroption">
										<?php if($houseRules): ?><h3>House Rules</h3><?php endif;?>
										<?php echo $houseRules; // Dynamic Content ?>
									</div>
								</div>
							</div>
							<br>
						</div>
					</div>
				</div>
			</section>
			<br>
			<br>
			<?php get_template_part('include/map');?>
			<section class="unitMap">
				<div class="container">
					<div class="row">
						<div class="col">
							<h3>Point of Interest</h3>
							<?php 

							$location = get_field('map');

							if( !empty($location) ):
							?>
							<div class="acf-map">
								<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
							</div>
						<?php endif; ?>
						</div>
					</div>
				</div>

				<div class="contactFormMobile">
					<?php echo get_avatar( get_the_author_meta( 'ID' ),200); ?><br>
									<h5><?php the_author_meta( 'first_name'); ?> <?php the_author_meta( 'last_name'); ?></h5>
									<p><i class="fa fa-mobile"></i> +1-<?php the_author_meta( 'phone'); ?> </p>
									<a href="#"><i class="fa fa-envelope-o"></i> <?php the_author_meta( 'user_email'); ?></a>
					<?php
					$property_field = get_the_title();
					echo do_shortcode('[gravityform id="2" field_values="property='.$property_field.'" title="false" description="false"]');
					?>
				</div>
			</section>
		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php endif; ?>

	</section>
	<!-- /section -->
	</main>