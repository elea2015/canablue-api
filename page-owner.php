<?php /* Template Name: Owners */ get_header(); ?>
	
	<?php if(is_page(94)){
		get_template_part('include/videobg');
	}?>
	
	<?php if(!is_page(94)): ?>
	<section class="hero heroHome heroSell" style="background:url(<?php the_field('ownersBg')?>);background-size:cover; background-position:center;">
		<div class="container">
			<div class="row">
				<div class="col">
					<h1><?php the_field('service_hero');?></h1>
				</div>
			</div>
		</div>
		<img class="mouseIcon" src="<?php echo get_template_directory_uri(); ?>/img/icon/mouse.png">
	</section>
<?php endif; ?>

	<section id="ownersForm" class="container-fluid localExpert wow fadeInUp pb-0">
		<div class="row align-items-center justify-content-center">
			<div class="col-md-6">
				<h3 class="text-center blue"><?php the_field('service_pitch_title');?></h3>
				<p class="text-center"><?php the_field('service_pitch_message');?></p>
				<div class="ownersForm wow fadeInUp">
					<p><i class="fa fa-mobile"></i> 809-975-3900 </p>
					<a href="mailto:info@canablue.com"><i class="fa fa-envelope-o"></i> info@canablue.com</a><br>
					<a href="mailto:anamaria@canablue.com"><i class="fa fa-envelope-o"></i> anamaria@canablue.com</a>
					<?php
					$pageName = get_the_title();
					echo do_shortcode('[gravityform id="6" field_values="property='.$pageName.'" title="false" description="false" ajax="true"]');
					?>
				</div>
			</div>
		</div>
		<br><br><br>
		<div id="legal" class="row align-items-center bgBlue">
			<div class="col-md noPadding">
				<div class="serviceImg wow fadeInLeft">
					<img src="<?php the_field('service_image')?>">
				</div>
			</div>
			<div class="col-md noPadding servicePitch wow fadeInRight">
				<div class="servicePitchContent">
					<div class="ownersSericeList white"><?php the_field('service_list'); ?></div>
				</div>
			</div>
		</div>
		
		<div class="row align-items-center reverse">
			<div class="col-md noPadding servicePitch wow fadeInLeft">
				<div class="servicePitchContent">
					<div class="ownersSericeList"><?php the_field('service_list_2'); ?></div>
				</div>
			</div>
			<div class="col-md noPadding wow fadeInRight">
				<div class="serviceImg">
					<img src="<?php the_field('service_image_2')?>">
				</div>
			</div>
		</div>

		<div id="consultation" class="row align-items-center bgBlue">
			<div class="col-md noPadding">
				<div class="serviceImg wow fadeInLeft">
					<img src="<?php the_field('service_image_3')?>">
				</div>
			</div>
			<div class="col-md noPadding servicePitch wow fadeInRight">
				<div class="servicePitchContent">
					<div class="ownersSericeList white"><?php the_field('service_list_3'); ?></div>
				</div>
			</div>
		</div>
		
		<div class="row align-items-center reverse">
			<div class="col-md noPadding servicePitch wow fadeInLeft">
				<div class="servicePitchContent">
					<div class="ownersSericeList"><?php the_field('service_list_4'); ?></div>
				</div>
			</div>
			<div class="col-md noPadding wow fadeInRight">
				<div class="serviceImg">
					<img src="<?php the_field('service_image_4')?>">
				</div>
			</div>
		</div>
		
	</section>

	<section class="optin">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<a class="btn btn-primary" href="#ownersForm">Contact us</a>
				</div>
			</div>
		</div>
	</section>

		<!-- Services -->
	<?php get_template_part('include/services'); ?>

	<!-- optin -->
	<?php get_template_part('include/optin'); ?>

	<?php if(is_page(3326)): ?>
	<style>
		.bgBlue{
			background:#1b4073;
		}

		.white{
			color:white;
		}
		@media(max-width: 768px){
			.reverse div:nth-child(1){
				order:2;
			}
		}
	</style>
<?php endif ?>

<?php get_footer(); ?>