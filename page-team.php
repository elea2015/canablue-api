<?php /* Template Name: Team*/ get_header(); ?>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>


	<section class="buySection">
		<div class="container text-center">
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8 wow fadeInUp">
					<h2><?php the_title();?></h2>
					<p>We love what we do, we are canablue</p>
				</div>
			</div>
		</div>
	</section>


<section class="aboutPamela">
		<div class="container">
			<?php if( have_rows('members') ): ?>

			<?php while( have_rows('members') ): the_row(); 

				// vars
				$name = get_sub_field('name');
				$nameFormated = str_replace(' ', '', $name);
				$bio = get_sub_field('bio');
				$picture = get_sub_field('picture');

				?>

				<div class="row">
					<div class="col-md-4">
						<img class="wow fadeInLeft" src="<?php echo $picture;?>">
						<div class="pt-5 text-center">
							<a href="mailto:<?php echo strtolower($nameFormated);?>@canablue.com" class="text-lowercase blue"><?php echo $nameFormated;?>@canablue.com</a><br><br>
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#<?php echo strtolower($nameFormated);?>Modal">Work With <?php echo $name;?></button>
						</div>
					</div>
					<div class="col-md-8 wow fadeInRight">
						<h4>About <?php echo $name;?></h4>
						<p><?php echo $bio;?></p>
					</div>
				</div>

					<!-- Modal -->
				<div class="modal fade" id="<?php echo strtolower($nameFormated);?>Modal" tabindex="-1" role="dialog" aria-labelledby="<?php echo strtolower($nameFormated);?>ModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="<?php echo strtolower($name);?>ModalLabel">Work With <?php echo $name;?></h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
			        <?php echo do_shortcode('[gravityform id="5" field_values="agent='.$name.'" title="false" description="false"]'); ?>
			     	 </div>
				    </div>
				  </div>
				</div>


			<?php endwhile; ?>


		<?php endif; ?>
			
		</div>
	</section>
	

<?php endwhile; endif;  ?>

	<?php get_template_part('include/optin'); ?>

	<?php get_template_part('include/zonas')?>

<?php get_footer(); ?>