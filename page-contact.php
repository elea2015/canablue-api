<?php /* Template name: Contact */ get_header() ?>

<section class="contact-heading">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="contact-heading text-center">
                    <h1 class="blue">Contact us today</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact-form py-3">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="contact-form">
                    <?php echo do_shortcode('[gravityform id="4" title="false" description="false"]');?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="contact-address">
                    <strong class="blue">Address</strong><br>
                    Avenida Barcelo Plaza San Juan Shopping Center, Local P104, Punta Cana.<br>
                    <a target="_blank" class="blue" href="https://goo.gl/maps/FEV2uPxA4hrsWhBd9">Get directtions</a>
                </div>
                <div class="contact-hours mt-4">
                    <strong class="blue">Business hours</strong><br>
                    Monday, 9:30AM–6PM<br>
                    Tuesday, 9:30AM–6PM<br>
                    Wednesday, 9:30AM–6PM<br>
                    Thursday, 9:30AM–6PM<br>
                    Friday, 9:30AM–6PM<br>
                    Saturday, 9:30AM–12PM<br>
                    Sunday, Closed
                </div>
            </div>    
        </div>
    </div>
</section>

<section class="contact-map">
    <?php get_template_part('include/map');?>
    <?php 
        $location_map = get_field('map');

        if( !empty($location_map) ):
        ?>
        <div class="acf-map">
            <div class="marker" data-lat="<?php echo $location_map['lat']; ?>" data-lng="<?php echo $location_map['lng']; ?>"></div>
        </div>
    <?php endif; ?>	
</section>


<?php get_footer(); ?>